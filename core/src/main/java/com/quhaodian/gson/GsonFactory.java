package com.quhaodian.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Date;

public class GsonFactory {
  
  public Gson gson() {
    Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,
        new DateTypeAdapter2()).create();
    return gson;
    
  }
}
